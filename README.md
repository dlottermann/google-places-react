

## Google Places ReactJS
 
O seguinte projeto tem como objetivo desenvolver uma solução o mais completa possível utilizando as mais recentes tecnologias com base em ReactJs e demais bibliotecas.

O objetivo principal do projeto é efetuar consultas a API graphql disponibilizada e efetuar operação de pesquisa e listagem na mesma, alem de possibilitar a edição client-side dos itens.

A solução elaborada possui as seguintes tecnologias:

 - ReactJS
 - Google Maps API
 - Browser geolocation
 - Google geocode
  
## Como iniciar o projeto

### Clonar repositório

Siga os seguintes passos para iniciar :<br />

-- Clone o repositório no seu diretório de preferência. <br />

```sh
$ git clone https://gitlab.com/dlottermann/google-places-react.git
```

-- Entre no diretorio do projeto pelo terminal de comando. <br />

```sh
$ cd google-places-react
```

-- Execute o comando  `yarn` ou `npm install` para instalar todas as dependências necessárias. <br />

```sh
$ yarn
```

-- Antes de inicializar o projeto deve ser configurada a chave* da API do Google Maps no arquivo .env da raiz do projeto

* A chave pode ser obtida no site do próprio google - [https://console.developers.google.com/](https://console.developers.google.com/), existe uma cota gratuita para desenvolvimento - sem custo nenhum 

-- Depois de todas as dependencias instaladas, e a chave da API Google Maps configurada rode o comando `yarn start` para inicializar o projeto.

```sh
$ yarn start
```


## Funcionalidades

 - Dashboard com exibição do mapa e possibilidade de adicionar pontos através da latitude e longitude
 - O componente InputMarker também possibilita a adicionar novos pontos no mapa através de endereços por extenso ( ex: Avenida Brasil, 1102,São Paulo) -  basta passar a prop address como booleano **true**
 