import React, { useState } from 'react'
import { PlaceMap } from './PlaceMap'

import Geocode from 'react-geocode'

const fetchAddress = async (address) => {
  Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAPS_KEY)
  if (address === null) return

  const response = await Geocode.fromAddress(address)
  const { lat, lng } = response.results[0].geometry.location
  return { lat, lng }
}

// To-do add mock places
// import { places } from "../mocks";

export const InputMarker = ({ coordinates = true, address = false }) => {
  const [places, setPlaces] = useState([])
  const [addr, setAddr] = useState('')
  const [info, setInfo] = useState('')
  const [coord, setCoord] = useState({
    lat: '',
    long: ''
  })

  const cleanFields = key => key === 'address' ? setCoord({ lat: '', long: '' }) : setAddr('')

  const handlePlaces = async () => {
    if (addr !== '') {
      const { lat, lng } = await fetchAddress(addr)
      setPlaces([...places, { id: new Date().getTime(), latitude: lat, longitude: lng, information: info }])
    }

    if ((coord.lat !== 0 && coord.lat !== '') && (coord.long !== 0 && coord.long !== '')) {
      setPlaces([...places, { id: new Date().getTime(), latitude: parseFloat(coord.lat), longitude: parseFloat(coord.long), information: info }])
    }
  }

  return (
    <>
      <div className='container-form'>
        <h4>Adicione um local</h4>

        {coordinates &&
          <div className='box-coordinates'>
            <div className='item'>
              <input
                data-testid='input-latitude'
                type='text'
                className='coordinates'
                placeholder='Latitude'
                value={coord.lat}
                onFocus={() => cleanFields('lat')}
                onChange={e => setCoord({ ...coord, lat: e.target.value })}
              />
              <small>Informe a latitude (ex: -34.3920927)</small>
            </div>
            <div className='item'>
              <input
                data-testid='input-longitude'
                type='text'
                className='coordinates'
                placeholder='Longitude'
                value={coord.long}
                onFocus={() => cleanFields('long')}
                onChange={e => setCoord({ ...coord, long: e.target.value })}
              />
              <small>Informe a longitude (ex: -54.12093892)</small>
            </div>
          </div>}

        {(coordinates && address) && <h5>OU</h5>}

        {address &&
          <div>
            <input
              data-testid='input-address'
              type='text'
              id='address-bar'
              value={addr}
              placeholder='Digite um novo local...'
              onFocus={() => cleanFields('address')}
              onChange={e => setAddr(e.target.value)}
            />
            <small>
              Informe um endereço (ex: Rua Capitão Eleutério, 321, Passo Fundo)
            </small>
          </div>}

        <textarea
          id='info-bar'
          placeholder='Adicione alguma informação do local'
          onChange={e => setInfo(e.target.value)}
        />

        <button
          data-testid='btn-add'
          type='button'
          className='btn-add'
          onClick={handlePlaces}
        >
          Adicionar local
        </button>

        <PlaceMap markers={places} />

      </div>

    </>
  )
}
