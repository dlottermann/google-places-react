import React, { useEffect, useState } from 'react'
import { GoogleMap, useLoadScript, Marker, InfoWindow } from '@react-google-maps/api'
import ReactLoading from 'react-loading'

const containerStyle = {
  width: '100%',
  height: '70vh'
}

export const PlaceMap = ({ markers }) => {
  const [center, setCenter] = useState({
    lat: -30.0277,
    lng: -51.2287,
    zoom: 8
  })
  const [info, setInfo] = useState(null)

  useEffect(() => {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(function (position) {
        setCenter({ lat: position.coords.latitude, lng: position.coords.longitude, zoom: 10 })
      })
    } else {
      console.log('Not Available')
    }
  }, [])

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_KEY
  })

  if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>
  }

  const renderMap = () => {
    return (
      <>
        <div className='container-map'>
          <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={center.zoom}
          >
            {markers.map((item, idx) => {
              return (
                <Marker
                  key={item.id}
                  position={{ lat: item.latitude, lng: item.longitude }}
                  onClick={() => setInfo(item)}
                />)
            })}

            {info
              ? (
                <InfoWindow
                  style={{ color: 'red' }}
                  position={{ lat: info.latitude, lng: info.longitude }}
                  onCloseClick={() => setInfo(null)}
                >
                  <div>
                    <h5>{info.information !== '' ? info.information : 'Nenhuma informação adicionada'}</h5>
                  </div>
                </InfoWindow>)
              : null}

          </GoogleMap>

        </div>
      </>
    )
  }

  return isLoaded
    ? renderMap()
    : (
      <ReactLoading
        type='spin'
        width={25}
        className='loader'
        color='#3BAEA2'
      />
    )
}
