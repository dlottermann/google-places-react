import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { InputMarker } from './InputMarker'

describe('Test component InputMarker', () => {
  test('Render component!', () => {
    render(<InputMarker />)
  })

  test('Test input values!', () => {
    render(<InputMarker />)

    const lat = screen.getByTestId('input-latitude')
    fireEvent.change(lat, { target: { value: '-30.0277' } })
    expect(lat.value).toBe('-30.0277')

    const lng = screen.getByTestId('input-longitude')
    fireEvent.change(lng, { target: { value: '-51.2287' } })
    expect(lng.value).toBe('-51.2287')
  })

  test('Test address values!', () => {
    render(<InputMarker address />)

    const address = screen.getByTestId('input-address')
    fireEvent.change(address, { target: { value: 'Rua Capitão Eleutério, 857, Passo Fundo - RS' } })
    expect(address.value).toBe('Rua Capitão Eleutério, 857, Passo Fundo - RS')
  })
})
