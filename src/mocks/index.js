export const places = [
  {
    id: 4314902,
    nome: 'Porto Alegre',
    latitude: -30.0318,
    longitude: -51.2065,
    capital: true,
    codigo_uf: 43
  },
  {
    id: 4313409,
    nome: 'Novo Hamburgo',
    latitude: -29.6875,
    longitude: -51.1328,
    capital: false,
    codigo_uf: 43
  },
  {
    id: 4311403,
    nome: 'Lajeado',
    latitude: -29.4591,
    longitude: -51.9644,
    capital: false,
    codigo_uf: 43
  }
]
