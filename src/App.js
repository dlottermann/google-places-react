import React from 'react'
import { InputMarker } from './components'

import './assets/css/styles.css'

export default function App () {
  return (
    <div className='container'>
      <InputMarker />
    </div>
  )
}
